
terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.1.0"
    }
  }
}

provider "google" {
}

variable "image_tag" {
  type    = string
  default = "latest"
}

resource "google_cloud_run_service" "default" {
  name     = "jug-hh-12-2021"
  location = "europe-west1"

  template {
    spec {
      containers {
        image = "europe-west1-docker.pkg.dev/hermesgermany/hsi/jug-hh-12-2021:${var.image_tag}"
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}