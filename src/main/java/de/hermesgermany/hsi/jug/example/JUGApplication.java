package de.hermesgermany.hsi.jug.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JUGApplication {

  public static void main(String[] args) {
    SpringApplication.run(JUGApplication.class, args);
  }
}
