package de.hermesgermany.hsi.jug.example;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

  @GetMapping(path = "/greetings")
  public String greet() {
    return "Hello, Java User Group Hamburg!";
  }
}
